# Toy brick-style constructor, 3d-printable

It's designed from scratch to be 3D-printable on typical FDM printers.
As a result it's not compatible with any other widely available
constructors.

I've made it tolerant to imprecisions by designing most parts to have
springy connections, so effects of extruder miscalibration, filament
diameter variations are minimized.

Since a lot of parts have thickness of one line width, special care
was taken to make sure that slicer produces high-quality printing sequence.
I'm using PrusaSlicer/SuperSlicer, so the models are optimized for them.

Bricks lock with a pleasant click, and things built from them are very sturdy.
If you feel that forces required to lock-unlock are too high, you can
easily modify grip strength in source files.


4x2 brick:
![](images/4x2_top.png)

Bottom view of the same brick:
![](images/bottom_connector.png)


## Building STL files

You need the following:
 - Python 3
 - solidpython (`pip install solidpython`)
 - OpenSCAD
 - sconstruct (optional)

When you've installed those packages, run:

    python3 constructor.py  # will produce .scad files in scad/
    scons -j <num_of_cpus>  # will render scad/<xx>.scad files to out/<xx>.stl


## Slicing and Printing

All models are optimized for PrusaSlicer / SuperSlicer, you can try to use
other slicers, though.

Slicer/printer settings:
 - 0.4 mm nozzle,
 - 0.42..0.44 mm line width,
 - PrusaSlicer: Print Settings > Advanced > Slice gap closing radius = 0.002 mm.
   Otherwise the slicer will ignore tiny gaps that were strategically placed
   in the right places to force slicer generate a good printing sequence.
 - first layer height: 0.2 mm
 - other layers height: 0.08 .. 0.16 mm should work reasonably well.
   
Bottom of a properly sliced part should look like this:

![](images/sliced.png)

On materials:
 - ABS works the best. It has low creep, and connection grip stays mostly
   the same. I'm printing all parts on a Prusa-kinematics-type printer
   without any problems.
 - PLA: creep at the room temperature is too large, also PLA is quite rigid.
 - PETG: creep is better than PLA, and it's reasonably flexible. YMMV.
