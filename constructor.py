#!/usr/bin/python3
from random import random
from typing import Tuple
from math import sqrt

from solid import (
    scad_render_to_file, translate, linear_extrude, rotate_extrude,
    minkowski, hull, union, cube, intersection, polygon, rotate
)
from solid.utils import up
from solid.objects import cylinder, sphere, circle

D = 0.01
D2 = D * 2
SLICER_GAP = 0.01

LW = 0.42

SIZE = 8
WALL_TH = LW
TOP_TH = 1.0

BUMP_H = 2.2
BUMP_SIZE = 5
BUMP_GAP = 0.00
BUMP_PROFILE = [
    (-0.05, 0),
    (0.0, 0.44),
    (0.25, BUMP_H * 0.5),
    (-0.1, BUMP_H * 0.9),
    (-0.6, BUMP_H),
]
HOLE_PROFILE = [
    (0.15, 0), # accidental elephant foot comp
    (0.15, 0.2),
    (0.0, 0.204),
    (0.0, 0.44),
    (0.25, BUMP_H * 0.5),
    (0.25, BUMP_H),
]

def bump(profile, r, positions):
    res = union()
    for i, (x1, z1), (x2, z2) in zip(range(len(profile)), profile, profile[1:]):
        first = (i == 0)
        last = (i == len(profile) - 2)
        spike = cylinder(
            r1=x1 + r,
            r2=x2 + r,
            h=z2 - z1 + D * int(not first),
            segments=30
        )
        spikes = union()
        for pos in positions:
            spikes += translate(pos + (z1 - int(not first) * D,))(spike)
        res += hull()(spikes)

    return res


def conn_male():
    # TODO: make it octahedron?
    edge_offset = 0.2
    r = 1.5
    profile = [(BUMP_PROFILE[0][0], -D)] + BUMP_PROFILE

    spike = rotate_extrude(segments=40)(polygon(
        [(x + r, h) for x, h in profile] +
        [(0, profile[-1][1]), (0, profile[0][1])]
    ))

    off1 = BUMP_SIZE/2 - r
    off2 = BUMP_SIZE/2 - r - edge_offset
    positions = (
        [(xi * off1, yi * off2) for xi in (-1, 1) for yi in (-1, 1)] +
        [(xi * off2, yi * off1) for xi in (-1, 1) for yi in (-1, 1)]
    )
    return translate([SIZE/2, SIZE/2])(bump(profile, r, positions))


def conn_female():
    th = LW
    r = 1.0

    def volume(sub=False):
        if sub:
            profile = [(0, -D)] + HOLE_PROFILE + [(0, HOLE_PROFILE[-1][1] + .5)]
        else:
            profile = HOLE_PROFILE

        off = BUMP_SIZE/2 + BUMP_GAP - r
        if not sub:
            off += th

        positions = [(xi * off, yi * off) for xi in (-1, 1) for yi in (-1, 1)]
        return bump(profile, r, positions)

    attach_th = LW

    off1 = BUMP_SIZE/2 - 1.0 + BUMP_GAP
    off2 = SIZE/2 - WALL_TH - SLICER_GAP

    attach0 = translate([off1, off1])(cube([off2 - off1, off2 - off1, BUMP_H]))
    attach0_cut = translate([off1 + attach_th, off1 + attach_th, -D])(
        cube([off2 - off1 - attach_th * 2, off2 - off1 - attach_th * 2,
              BUMP_H + D2]))

    attach = union()
    attach_cut = union()
    for a in 0, 90, 180, 270:
        attach += rotate(a)(attach0)
        attach_cut += rotate(a)(attach0_cut)

    # attach = rotate(45)(translate([0, 0, BUMP_H/2])(
    #     cube([SIZE * sqrt(2), attach_th * 2 + attach_gap, BUMP_H], center=True)
    # ))
    # attach += rotate(90)(attach)

    # attach_cut = rotate(45)(translate([0, 0, BUMP_H/2])(
    #     cube([SIZE * sqrt(2) - 2, attach_gap, BUMP_H + D2], center=True)
    # ))
    # attach_cut += rotate(90)(attach_cut)

    pos = volume(False) + attach
    neg = volume(True) + attach_cut
    return translate([SIZE/2, SIZE/2])(intersection()(
        pos - neg,
        cube([SIZE - D2, SIZE - D2, BUMP_H * 2 + D2], center=True)
    ))


def unit(h):
    a = translate([-SIZE/2, -SIZE/2])(
        cube([SIZE, WALL_TH, h]) +
        translate([0, SIZE-WALL_TH])(cube([SIZE, WALL_TH, h])) +
        cube([WALL_TH, SIZE, h]) +
        translate([SIZE-WALL_TH, 0])(cube([WALL_TH, SIZE, h])) +
        translate([0, 0, h - TOP_TH])(cube([SIZE, SIZE, TOP_TH]))
    )
    a += conn_female()
    a += up(h)(conn_male())
    return a


def brick(xn, yn, h):
    a = (
        cube([SIZE * xn, WALL_TH, h]) +
        translate([0, SIZE * yn - WALL_TH])(cube([SIZE * xn, WALL_TH, h])) +
        cube([WALL_TH, SIZE * yn, h]) +
        translate([SIZE * xn - WALL_TH, 0])(cube([WALL_TH, SIZE * yn, h])) +
        translate([0, 0, h - TOP_TH])(cube([SIZE * xn, SIZE * yn, TOP_TH]))
    )

    for xi in range(xn):
        for yi in range(yn):
            a += translate([xi * SIZE, yi * SIZE])(conn_female())
            a += translate([xi * SIZE, yi * SIZE, h])(conn_male())

    grid_h = max(5, h - D)
    for xi in range(xn - 1):
        a += translate([(xi + 1) * SIZE - WALL_TH, WALL_TH + SLICER_GAP])(
            cube([WALL_TH * 2, SIZE * yn - 2 * WALL_TH - 2 * SLICER_GAP, grid_h])
        )

    for yi in range(yn - 1):
        a += translate([WALL_TH + SLICER_GAP, (yi + 1) * SIZE - WALL_TH])(
            cube([SIZE * xn - 2 * WALL_TH - 2 * SLICER_GAP, WALL_TH * 2, grid_h])
        )
    return a


def main():
    # tr = unit(SIZE/2)

    for xn, yn in (1, 1), (2, 1), (2, 2), (4, 1), (4, 2), (8, 1):
        tr = brick(xn, yn, SIZE)
        scad_render_to_file(tr, f"scad/brick_{xn}x{yn}.scad", include_orig_code=False)

main()
